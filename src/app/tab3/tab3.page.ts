import { Component, OnInit } from '@angular/core';
import { InfoService } from '../services/info.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  oftal =[];

  constructor(private getD: InfoService) {}

  ngOnInit() {
    return this.getD.getOfta().subscribe( (res:any) => {
      this.oftal = res.data;
      console.log(this.oftal);
    })
  }

}
