import { Component, OnInit } from '@angular/core';
import { InfoService } from '../services/info.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  opticas=[];

  constructor(private getD: InfoService) {}

  ngOnInit() {
    return this.getD.getData().subscribe( (res:any) => {
      this.opticas = res.data;
      console.log(this.opticas);
    })
  }

}
