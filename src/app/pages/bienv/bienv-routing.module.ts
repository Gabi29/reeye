import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BienvPage } from './bienv.page';

const routes: Routes = [
  {
    path: '',
    component: BienvPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BienvPageRoutingModule {}
