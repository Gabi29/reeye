import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BienvPageRoutingModule } from './bienv-routing.module';

import { BienvPage } from './bienv.page';
import { HeaderModule } from '../../components/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BienvPageRoutingModule,
    HeaderModule
  ],
  declarations: [BienvPage]
})
export class BienvPageModule {}
