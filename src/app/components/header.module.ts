import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';
import { PopoverComponent } from './popover/popover.component';
import { HeaderbackComponent } from './headerback/headerback.component';



@NgModule({
  declarations: [
    HeaderComponent,
    PopoverComponent,
    HeaderbackComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    HeaderComponent,
    HeaderbackComponent
  ]
})
export class HeaderModule { }
