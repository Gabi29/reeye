import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Login } from '../models/login.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InfoService {

  URL="http://localhost:3000/";

  constructor(private http: HttpClient) { }

  getData(){
    return this.http.get("http://localhost:3000/innov/opticas/list");
  }

  getOfta(){
    return this.http.get("http://localhost:3000/innov/oftal/list");
  }

  login(login: Login):Observable<Request>{
    return this.http.post<Request>(`${this.URL}innov/login`,login);
  }
}
